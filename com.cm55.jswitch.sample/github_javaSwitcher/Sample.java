package github_javaSwitcher;

import static com.sun.jna.platform.win32.WinReg.*;

import com.sun.jna.platform.win32.*;

public class Sample {

  public static void main(String[]args) {
    Advapi32Util.registryCreateKey(HKEY_CLASSES_ROOT, ".00001\\00002");
    
    Advapi32Util.registrySetStringValue(HKEY_CLASSES_ROOT, ".00001\\00002", "", "123");
    Advapi32Util.registrySetStringValue(HKEY_CLASSES_ROOT, ".00001\\00002", "a", "abc");
    
    
    System.out.println(Advapi32Util.registryGetStringValue(HKEY_CLASSES_ROOT, ".00001\\00002", "a"));
    try {
      System.out.println(Advapi32Util.registryGetStringValue(HKEY_CLASSES_ROOT, ".00001\\00002", "b"));
    } catch (Win32Exception ex) {
      System.out.println(ex.getMessage());
    }
    try {
      System.out.println(Advapi32Util.registryGetStringValue(HKEY_CLASSES_ROOT, ".00001\\11111", "a"));
    } catch (Win32Exception ex) {
      System.out.println(ex.getMessage());
    }

  }
}
