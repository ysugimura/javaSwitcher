package com.cm55.jswitch;

import java.io.*;
import java.nio.file.*;
import java.util.*;
import java.util.stream.*;

import com.cm55.sg.*;

/**
 * 設定パネル
 * @author ysugimura
 */
public class SettingsPanel {

  SgGroupBox box;
  SgTable<String>pathTable;
  
  public SettingsPanel() {
    
    pathTable = new SgTable<>(
      new SgTextColumn<String>("Path", s->s).setPreferredWidth(200),
      new SgTextButtonColumn<String>("Delete",  s->"Delete")
        .setOnClicked(index->delete(index)).setPreferredWidth(50)            
    );
    
    box = new SgGroupBox("Search Path", new SgBorderLayout.V(
       null,
       pathTable,
       new SgButton("Add", this::addPath)
    ));
    
    loadSearchPaths();
  }

  /** サーチパスを取得し、テーブルに設定する */
  private void loadSearchPaths() {
    pathTable.setRows(
      Arrays.stream(SysProperties.INSTANCE.getProperty(SysProperties.SEARCH_PATH).split(File.pathSeparator))
    );
  }
  
  /** 指定パスを削除する */
  private void delete(int index) {
    String row = pathTable.getRow(index);
    if (!SgYesNoDialog.show(pathTable,  "Deleting a path",  "Delete " + row + " ?")) return;    
    pathTable.removeRow(index);    
    saveSearchPaths();
  }

  /** パスを追加する */
  private void addPath(SgButton button) {
    Path path = SgDirectoryChooser.choose(pathTable);
    if (path == null) return;
    pathTable.addRow(path.toString());
    saveSearchPaths();
  }

  /** サーチパスをセーブする */
  private void saveSearchPaths() {
    SysProperties.INSTANCE.setProperty(
      SysProperties.SEARCH_PATH,
      pathTable.getRows().collect(Collectors.joining(File.pathSeparator))
    );
  }
  
  /** このパネルコンポーネントを取得する */
  SgComponent<?> getComponent() {
    return box;
  }
}
