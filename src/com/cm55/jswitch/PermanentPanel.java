package com.cm55.jswitch;

import com.cm55.jswitch.intf.*;
import com.cm55.sg.*;

public class PermanentPanel {

  SgTextArea textArea;
  PermanentModel model;
  
  public PermanentPanel() {
    this.model = Models.permanentModel;
    textArea = new SgTextArea().setEditable(false).setText(model.getText());
  }
  
  public SgComponent<?> getComponent() {
    return textArea;
  }
}
