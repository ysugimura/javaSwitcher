package com.cm55.jswitch.intf;

public abstract class PermanentModel {

  public static final String[]ENV_VARS = new String[] {
      "_JAVA_OPTIONS",
      "JAVA_TOOL_OPTIONS",
      "JAVA_OPTS",
      "JAVA_HOME",
      "JRE_HOME"
  };
  
  public abstract String getText();

  protected String getEnvs() {
    StringBuilder s = new StringBuilder();
    for (String envVar: ENV_VARS) {
      s.append(envVar + "=" + System.getenv(envVar) + "\n");
    }
    return s.toString();
  }
}
