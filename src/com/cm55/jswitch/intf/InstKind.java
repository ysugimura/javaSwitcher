package com.cm55.jswitch.intf;

public enum InstKind {
  JRE("JRE"),
  JDK("JDK");
  public final String name;
  private InstKind(String name) {
    this.name = name;
  }
}
