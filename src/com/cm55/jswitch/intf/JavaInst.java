package com.cm55.jswitch.intf;

import java.io.*;
import java.nio.file.*;

/**
 * Javaインストレーション情報
 * @author ysugimura
 */
public class JavaInst {

  /** インストレーション種類。JRE/JDK */
  public final InstKind kind;
  
  /** インストレーショントップパス */
  public final Path topPath;
  
  /** 備考 */
  public final String remarks;
  
  public JavaInst(InstKind kind, Path topPath, String remarks) {
    this.kind = kind;
    this.topPath = topPath;
    this.remarks = remarks;
  }
  
  /** このJavaインストレーションのbinパスを取得する */
  public Path getBinPath() {
    return topPath.resolve("bin");
  }
}
