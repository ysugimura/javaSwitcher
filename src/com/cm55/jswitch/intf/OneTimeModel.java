package com.cm55.jswitch.intf;

import java.io.*;
import java.util.*;

public interface OneTimeModel {

  /** 
   * 全インストレーションスロットを取得する
   * @return
   */
  List<JavaInst> getInstSlots();

  /** 
   * 指定されたパスのJREで.jarファイルを実行する。
   * その際、カレントディレクトリを.jarファイルのあるディレクトリにする。
   * バッチファイルを使う以外の一時的ディレクトリ変更方法はわからない。
   * @param jarFile 実行対象の.jarファイル
   * @param jrePath 実行に使用するjreのbinのパス
   * @throws Exception プロセス実行時例外
   */
  void executeJar(File jarFile, JavaInst slot) throws Exception;

  /** 
   * シェルウインドウを開く。
   * Windowsの場合いわゆるコマンドプロンプトだが、選択されたJREがパスに設定されているものとする。
   * @param jrePath　パスに設定するJREのbinディレクトリ
   * @throws Exception プロセス実行時例外
   */
  void openShell(JavaInst slot) throws Exception;

}