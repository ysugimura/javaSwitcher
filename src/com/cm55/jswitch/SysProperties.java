package com.cm55.jswitch;

import java.io.*;
import java.nio.file.*;
import java.util.*;
import java.util.stream.*;

/**
 * JRE/JDKサーチパス
 * @author ysugimura
 */
public class SysProperties {

  public static final SysProperties INSTANCE = new SysProperties();
  public static final String SEARCH_PATH = "SearchPath";

  private Properties properties;
  
  private SysProperties() {
    Path path = getPropertiesPath();
    try {
    List<String>list = Files.readAllLines(path);
    } catch (Exception ex) {
      
    }
  }
  
  /** プロパティを取得する */
  public String getProperty(String name) {
    ensureProperties();
    return (String)properties.get(name);
  }

  /** プロパティを設定する */
  public void setProperty(String name, String value) {
    ensureProperties();
    properties.put(name,  value);    
    saveProperties();
  }

  /** プロパティが未ロードであればロードする */
  private void ensureProperties() {    
    if (properties != null) return;
    Path path = getPropertiesPath();
    if (Files.exists(path)) {
      properties = new Properties(); 
      try {
        properties.load(Files.newBufferedReader(path));
      } catch (IOException ex) {
        // do nothing
      }
    } else {
      createDefaultProperties();
    }    
  }

  /** プロパティを書き込む */
  private void saveProperties() {
    Path path = getPropertiesPath();
    Path dir = path.getParent();
    if (!Files.exists(dir)) {
      try {
        Files.createDirectories(dir);
      } catch (IOException ex) {
        System.err.println("Could not create dir:" + dir);
        return;
      }
    }
    try {
      properties.store(Files.newBufferedWriter(path), "");
    } catch (IOException ex) {
      System.err.println("Could not write file:" + path);
    }
  }
  
  /** user.homeの下の設定用フォルダ名称 */
  private static final String SETTING_DIR = ".javaSwitcher";
  
  /** プロパティファイル名称 */
  private static final String PROPERTIES = "settings.properties";
  
  /** プロパティファイルのパスを取得する */
  private Path getPropertiesPath() {
    return Paths.get(System.getProperty("user.home"), SETTING_DIR, PROPERTIES);
  }
  
  private static final String[]JRE_PATHS = new String[] {
    "C:\\Program Files\\Java", 
    "C:\\Program Files (x86)\\Java"
  };

  /** propertiesオブジェクトにデフォルト値を設定する */
  private void createDefaultProperties() {
    properties = new Properties();
    properties.put(
      SEARCH_PATH, 
      Arrays.stream(JRE_PATHS).collect(Collectors.joining(File.pathSeparator))
    );
  }
}
