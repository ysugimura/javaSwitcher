package com.cm55.jswitch;

import java.awt.*;
import java.awt.datatransfer.*;
import java.awt.dnd.*;
import java.io.*;
import java.util.List;

import javax.swing.*;

import com.cm55.jswitch.intf.*;
import com.cm55.sg.*;

public class OneTimePanel {

  OneTimeModel model;
  List<JavaInst>slots;
  SgPanel upperPanel;
  SgTable<JavaInst>table;
  
  OneTimePanel() {
    this.model = Models.oneTimeModel;

    // drop panel
    SgPanel lowerPanel = new SgPanel(new SgBorderLayout.V(
        null,
        createDropPanel(),
        createShellButton()
    ));    
    upperPanel = new SgPanel(new SgBorderLayout.V(
      null,
      createJreTable(),
      lowerPanel
    ));    
    loadJreSlots();
  }
  
  void reload() {
   loadJreSlots();
  }
  
  SgComponent<?> getComponent() {
    return upperPanel;
  }
  
  /** JRE一覧テーブル */
  SgComponent<?> createJreTable() {
    table = new SgTable<>(
      new SgTextColumn<JavaInst>("Kind", s->s.kind.name).setPreferredWidth(100),
      new SgTextColumn<JavaInst>("Path", s->s.topPath.toAbsolutePath().toString()).setPreferredWidth(500),
      new SgTextColumn<JavaInst>("Env", s->s.remarks).setPreferredWidth(100)
    );
    return table;
  }
  
  void loadJreSlots() {
    slots = model.getInstSlots();        
    table.setRows(slots.stream());
  }
  
  /** Jarファイルドロップパネル */
  SgComponent<?> createDropPanel() {
    SgLabel dropPanel = 
      new SgLabel("Drop Jar file here").setHorAlign(SgHorAlign.CENTER)
      .setPreferredSize(new Dimension(400, 100));
    dropPanel.w().setDropTarget(new DropTarget() {
      @Override
      public synchronized void drop(DropTargetDropEvent dtde) {
        Transferable transfer = dtde.getTransferable();
        if (!transfer.isDataFlavorSupported(DataFlavor.javaFileListFlavor))
          return;
        dtde.acceptDrop(DnDConstants.ACTION_COPY_OR_MOVE);
        try {
          @SuppressWarnings("unchecked")
          List<File> fileList = (List<File>)transfer.getTransferData(DataFlavor.javaFileListFlavor);
          JavaInst jrePath = getJrePath();
          if (jrePath == null) return;
          model.executeJar(fileList.get(0), jrePath);
        } catch (Exception ex) {
          error(ex.getMessage());
        }
      }
    }); 
    return dropPanel;
  }

  /** シェルボタン */
  SgComponent<?> createShellButton() {
    return new SgButton("shell", b-> {
      JavaInst jrePath = getJrePath();
      if (jrePath == null) return;
      try {
        model.openShell(jrePath); 
      } catch (Exception ex) {
        error(ex.getMessage());
      }      
    });
  }
  
  /** 実行時のJREパスを取得する */
  JavaInst getJrePath() {
    int index = table.getSelectionModel().getSelectionIndex();
    if (index < 0) {
      error("JRE not selected");
      return null;
    }
    return slots.get(index);
  }
  
  /** エラーメッセージを表示 */
  void error(String message) {
    JOptionPane.showMessageDialog(null, message, "Error", JOptionPane.ERROR_MESSAGE);
  }
}
