package com.cm55.jswitch.win;

import java.io.*;
import java.util.*;

import com.cm55.jswitch.intf.*;

/**
 * 一時的環境変数を設定する。
 * {@link ProcessBuilder}に対して一時的な環境を設定する。
 * <ul>
 * <li>指定インストレーションがPathとなるようにする。
 * <li>JREの場合にはJRE_HOMEを設定する。
 * <li>JDKの場合にはJAVA_HOMEを設定する
 * </ul>
 * @author ysugimura
 */
public class TempEnv {
  
  /** Windowsの場合、PATHではなくPath */
  private static final String PATH = "Path";

  /** 
   * プロセスビルダに指定インストレーションを環境変数として設定する
   * @param builder プロセスビルダー
   * @param slot インストレーション
   */
  static void setEnv(ProcessBuilder builder, JavaInst slot) {    
    
    // プロセスビルダーの環境マップを取得
    Map<String, String> envMap = builder.environment();    

    // Pathを設定
    {
      String binPath = slot.getBinPath().toAbsolutePath().toString();
      String path = envMap.get(PATH);
      if (path != null) {
        path = binPath + File.pathSeparator + path;
      } else {
        path = binPath;
      }        
      envMap.put(PATH,  path);
    }

    // JAVA_HOMEもしくはJRE_HOMEを設定
    if (slot.kind == InstKind.JDK) {
      envMap.remove(StdEnv.JRE_HOME);
      envMap.put(StdEnv.JAVA_HOME,  slot.topPath.toAbsolutePath().toString());
    } else {
      envMap.remove(StdEnv.JAVA_HOME);
      envMap.put(StdEnv.JRE_HOME,  slot.topPath.toAbsolutePath().toString());
    }    
  }
}
