package com.cm55.jswitch.win;

import static com.sun.jna.platform.win32.WinReg.*;

import java.io.*;
import java.util.regex.*;

import com.cm55.jswitch.intf.*;
import com.sun.jna.platform.win32.*;


/**
 * シェルオープンコマンドを取得する
 * <p>
 * これは.jarファイルをダブルクリックした時に実行されるjavaw.exeを示している。
 * </p>
 * @author ysugimura
 */
public class ShellOpenCommand {

  private static final String DOT_JAR = ".jar";
  private static final String JARFILE = "jarfile";
  private static final String EXECUTABLE_JAR_FILE = "Executable Jar File";
  
  private static boolean initialized;
  
  /** 
   * シェルオープンコマンド
   * "C:\Program Files\Java\jre-10.0.2\bin\javaw.exe" -jar "%1" %*
   * のような文字列
   */
  private static String shellOpenCommand;
  
  /** 実行コマンド */
  private static File execCommand;
  
  /** binパス */
  private static File binPath;
  
  /** トップパス */
  private static File topPath;
  
  /** .jarのWindowsレジストリ情報「shell\open\command」を取得する */
  static String getJarShellOpenCommand() {
    ensure();
    return shellOpenCommand;
  }

  static File getTopPath() {
    ensure();
    return topPath;
  }
  
  private static Pattern pattern = Pattern.compile("^\\\"([^\\\"]+)\\\".*$");
  
  private static void ensure() {
    if (initialized) return;
    initialized = true;    
    
    // shellOpenCommandを取得する。    
    String progId = getStringDefaultValue(HKEY_CLASSES_ROOT, DOT_JAR);
    if (progId == null) return;    
    shellOpenCommand = getStringDefaultValue(HKEY_CLASSES_ROOT, progId + "\\shell\\open\\command");
    if (shellOpenCommand == null) return;

    // コマンド部分を抜き出す
    Matcher m = pattern.matcher(shellOpenCommand);
    if (!m.matches()) return;
    execCommand = new File(m.group(1));        
    
    binPath = execCommand.getParentFile();
    topPath = execCommand.getParentFile();
  }

  /** 指定されたインストレーションをオープンコマンドとして設定する */
  public static void setShellOpenInst(JavaInst inst) {
    
    // .jarの値であるprogIdを取得する
    String progId = getStringDefaultValue(HKEY_CLASSES_ROOT, DOT_JAR);
    if (progId == null) {
      // キーが存在しないか、値が存在しない。既にキーがあっても作成する。
      createKey(HKEY_CLASSES_ROOT, DOT_JAR);
      
      // 値を書き込む
      setStringDefaultValue(HKEY_CLASSES_ROOT, DOT_JAR, JARFILE);
      progId = JARFILE;      
    }

    // 
    createKey(HKEY_CLASSES_ROOT, progId);
    setStringDefaultValue(HKEY_CLASSES_ROOT, progId, EXECUTABLE_JAR_FILE);
    
    // キーを作成する
    String key = progId + "\\shell\\open\\command";
    createKey(HKEY_CLASSES_ROOT, key);
    
    // 値を設定する
    setStringDefaultValue(HKEY_CLASSES_ROOT, key, "\"C:\\Program Files\\Java\\jre-10.0.2\\bin\\javaw.exe\" -jar \"%1\" %*");
//    getStringValue(HKEY_CLASSES_ROOT, progId + "\\shell\\open\\command");    
  }

  /** 指定キーの規定値である文字列を読むこむ。キーが無い場合、値が無い場合のいずれもnullを返す。 */
  private static String getStringDefaultValue(HKEY root, String key) {
    try {
      return Advapi32Util.registryGetStringValue(root, key, "");
    } catch (Win32Exception ex) {
      // キーが無い場合にも値が無い場合にも同じ例外になる。
      return null;
    }
  }

  /** 指定キーの既定値として文字列を書き込む */
  private static void setStringDefaultValue(HKEY root, String key, String value) {
    try {
      Advapi32Util.registrySetStringValue(root, key, "", value);
    } catch (Win32Exception ex) {
      ex.printStackTrace();
    }
  }

  /** 
   * 指定されたキーを作成する。既に存在する場合でもエラーにはならない
   * 途中のキーが無い場合はそれらも作成される。例えば、jarfileキーが無い状態で、
   * jarfile\shell\open\commandを作成すれば、途中のすべてのキーが作成される。
   * @param root
   * @param key
   */
  private static void createKey(HKEY root, String key) {
    Advapi32Util.registryCreateKey(HKEY_CLASSES_ROOT, key);
  }

  
  public static void main(String[]args) {
    setShellOpenInst(null);
    
    
    ensure();
    System.out.println(shellOpenCommand);    
  }
}
