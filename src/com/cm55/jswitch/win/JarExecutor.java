package com.cm55.jswitch.win;

import java.io.*;

import com.cm55.jswitch.intf.*;

/**
 * 指定された.jarファイルを指定インストレーション環境で動作させる。
 * @author ysugimura
 */
public class JarExecutor {

  /**
   * .jarファイルを指定インストレーション環境で実行する
   * @param jarFile 実行対象のjarファイル
   * @param inst 実行環境として指定するJavaインストレーション
   * @throws Exception
   */
  static void execute(File jarFile, JavaInst inst)  throws Exception {
    final File tempBat = createBat(jarFile);
    executeBat(tempBat, inst);
  }

  /** 一時ディレクトリ */
  private static final File TMP_DIR = new File(System.getProperty("java.io.tmpdir"));
//  final File tmpDir = new File("c:\\users\\admin\\desktop"); テスト用
  
  
  /**
   * .jarファイルを起動するバッチファイルを作成する。
   * ただし、.jarファイルの存在するフォルダをカレントディレクトリにする。
   * @param jarFile 対象とする.jarファイル
   * @return 作成したバッチファイル
   * @throws Exception
   */
  private static File createBat(File jarFile) throws Exception {
    // 指定されたjarファイルのあるフォルダを"user.dir"とする。
    String userDir = jarFile.getParentFile().getAbsolutePath();

    // 一時ファイルとしてbatファイルを作成
    final File tempBat = File.createTempFile("jswitch", ".bat", TMP_DIR);
    BufferedWriter w = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(tempBat)));
    w.write("cd " + userDir + "\r\n");
    w.write("javaw -jar " + jarFile.getAbsolutePath() + "\r\n");
    w.close();
    return tempBat;
  }

  /**
   * バッチファイルを実行する。
   * バッチファイルではjavawのみで.jarファイルを起動するが、その際に環境変数を設定しておく必要がある。
   * Pathとして指定したインストレーションのbinパスが設定されるようにする。
   * 実行終了後にバッチファイルを消す。
   * @param tempBat 実行すつバッチファイル
   * @param inst　環境変数に設定するインストレーション
   * @throws Exception
   */
  private static void executeBat(final File tempBat, JavaInst inst) throws Exception {
    
    // batファイルを実行
    ProcessBuilder pb = new ProcessBuilder(tempBat.getAbsolutePath());
    TempEnv.setEnv(pb,  inst);
    final Process p = pb.start();    
    
    // プロセスの終了を待ち、batファイルを削除
    new Thread(new Runnable() { public void run() { 
      try {
        p.waitFor();
      } catch (InterruptedException ex) {}
      tempBat.delete();
    }}).start();    
  }
}
