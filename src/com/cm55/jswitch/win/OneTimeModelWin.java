package com.cm55.jswitch.win;

import java.io.*;
import java.util.*;

import com.cm55.jswitch.intf.*;

public class OneTimeModelWin implements OneTimeModel {
  
  /**
   * {@link JavaInst}のリストを取得する
   */
  @Override
  public List<JavaInst>getInstSlots() {
    return JavaInstLister.list();
  }
  
  /** {@inheritDoc} */
  @Override
  public void executeJar(File jarFile, JavaInst slot)  throws Exception {
    JarExecutor.execute(jarFile, slot);
  }
  
  /** {@inheritDoc} */
  @Override
  public void openShell(JavaInst slot) throws Exception {
    CmdPrompt.open(slot);
  }

}
