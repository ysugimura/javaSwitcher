package com.cm55.jswitch.win;

import com.cm55.jswitch.intf.*;

/**
 * コマンドプロンプトをオープンする。
 * @author ysugimura
 */
public class CmdPrompt {

  /** 新たなシェルウインドウをオープンするコマンド */
  private static final String[]OPEN_SHELL = new String[] {
    "cmd", "/c", "start"
  };
  
  /** 
   * 指定されたインストレーションを環境変数に指定し、コマンドプロンプトをオープンする
   * @param inst 環境変数に設定するインストレーション
   * @throws Exception
   */
  static void open(JavaInst inst) throws Exception {
    ProcessBuilder builder = new ProcessBuilder(OPEN_SHELL);
    TempEnv.setEnv(builder, inst);
    builder.start();
  }
}
