package com.cm55.jswitch.win;

import java.io.*;
import java.nio.file.*;
import java.util.*;
import java.util.stream.*;

import com.cm55.jswitch.*;
import com.cm55.jswitch.intf.*;

/**
 * Javaインストレーションののリストを作成する
 * <p>
 * Windowsでは、"C:\\Program Files\\Java"もしくは"C:\\Program Files (x86)\\Java"の下のフォルダがJava環境となる。
 * </p>
 * @author ysugimura
 */
public class JavaInstLister {

  /** JRE/JDKの調査パス */
  private static final String[]JRE_PATHS = new String[] {
    "C:\\Program Files\\Java", 
    "C:\\Program Files (x86)\\Java"
  };

  /** 
   * {@link SysProperties}に設定されているJRE/JDK検索パスに従い、JRE/JDKをリストアップする
   * @return
   */
  static List<JavaInst>list() { 
    List<JavaInst>paths = new ArrayList<JavaInst>();
    
    Arrays.stream(SysProperties.INSTANCE.getProperty(SysProperties.SEARCH_PATH).split(File.pathSeparator))
      .map(top->Paths.get(top))
      .flatMap(top-> {
        try {
          return Files.list(top);
        } catch (IOException ex) {
          return Stream.empty();
        }
      }).forEach(sub-> {
        getKind(sub).ifPresent(kind->paths.add(new JavaInst(kind, sub, "")));
      });

    Collections.sort(paths, new Comparator<JavaInst>() {
      public int compare(JavaInst a, JavaInst b) {
        return a.topPath.compareTo(b.topPath);
      }
    });
    
    return paths;
  }
  
  /** 
   * 指定されたフォルダについて。JRE/JDKその他を判別する
   * @param folder 対象フォルダ
   * @return isntKind.JRE/JDKあるいはempty
   */
  private static Optional<InstKind> getKind(Path folder) {
    Path bin = folder.resolve("bin");
    if (!Files.exists(bin) || !Files.isDirectory(bin)) return Optional.empty();
    if (!Files.exists(bin.resolve("java.exe")) ||
        !Files.exists(bin.resolve("javaw.exe"))) return Optional.empty();
    if (Files.exists(bin.resolve("javac.exe")))    
      return Optional.of(InstKind.JDK);
    else
      return Optional.of(InstKind.JRE);
  }
}
