package com.cm55.jswitch.win;

import java.io.*;

import com.cm55.jswitch.intf.*;

public class PermanentModelWin extends PermanentModel {
    
  public String getText() {
    return 
        "jar-shell-open-command=" + ShellOpenCommand.getJarShellOpenCommand() + "\n\n" +
        this.getEnvs() + "\n" +
        getPaths();
  }

  String getPaths() {
    StringBuilder s = new StringBuilder();
    s.append("Path=\n");
    for (String path: System.getenv("Path").split(File.pathSeparator)) {
      s.append("  " + path + "\n");
    }
    return s.toString();
  }
}
