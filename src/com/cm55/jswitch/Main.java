package com.cm55.jswitch;
import java.awt.*;

import javax.swing.*;
import javax.swing.table.*;

import com.cm55.jswitch.win.*;
import com.cm55.sg.*;

public class Main {
  

  DefaultTableModel tableModel;
  ListSelectionModel selectionModel;

  Main() {
    
    SgFrame frame =  new SgFrame(AppName.NAME + " " + Version.version)
        .setExitOnClose()
        .setBounds(50, 50, 400, 400)
        .setLayout(new BorderLayout());
    
    SgTabbedPane tabbedPane;
    OneTimePanel oneTimePanel;
    frame.setLayout(new SgBorderLayout.V(        
      createEnvLabel(),    
      tabbedPane = new SgTabbedPane()
        .addTab("One time", (oneTimePanel = new OneTimePanel()).getComponent())
        .addTab("Permanent", new PermanentPanel().getComponent())
        .addTab("Settings", new SettingsPanel().getComponent()),
      null
    ));
    
    tabbedPane.w().addChangeListener(l-> {
      if (tabbedPane.w().getSelectedIndex() == 0) oneTimePanel.reload();
    });
    
    frame.setVisible(true);
  }
  
  private SgLabel createEnvLabel() {
    return new SgLabel(
        System.getProperty("java.version") + " " +
        System.getProperty("os.arch") + " " + 
        System.getProperty("user.dir")
    );
  }


  public static void main(String[] args) throws Exception {
    String osName = System.getProperty("os.name").toLowerCase();
    if (osName.indexOf("windows") >= 0) {
      /*
      UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
      UIManager.setLookAndFeel("com.sun.java.swing.plaf.gtk.GTKLookAndFeel")
      */
      Models.oneTimeModel = new OneTimeModelWin();
      Models.permanentModel = new PermanentModelWin();
      new Main();
    } else {
      System.err.println("Currently only windows are supported");      
      System.exit(1);
    }
  }
}
